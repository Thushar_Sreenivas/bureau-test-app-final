import React, {useState, useContext, useEffect} from 'react';
import {SafeAreaView, StyleSheet, View, Text} from 'react-native';
import {Card, Button, TextInput} from 'react-native-paper';
import Discover from './screens/Discover';
import MySnackbar from './screens/Snackbar';
import {ViewSnackBar} from './Context';
import publicIP from 'react-native-public-ip';

export default function App() {
  const [userMobileNumber, setUserMobileNumber] = useState('');
  const [startTest, setStartTest] = useState(false);
  const [displaySnackBar, setDisplaySnackBar] = useContext(ViewSnackBar);
  const [ipAddressMob, setIpAddressMob] = useState('');
  const [mobNum, setMobNum] = useState('');
  const [ipAddressSuccess, setIsIpAddressSuccess] = useState(false);
  const testHandler = () => {
    setMobNum(userMobileNumber);
    if (ipAddressSuccess) {
      setStartTest(!startTest);
    }
  };
  useEffect(() => {
    function getIpAddress() {
      publicIP()
        .then((ip) => {
          setIpAddressMob(ip);
          setIsIpAddressSuccess(!ipAddressSuccess);
        })
        .catch((error) => {
          console.log(error);
        });
    }
    getIpAddress();
  }, []);
  return (
    // <ViewSnackBar.Provider value={viewSnackBar}>
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.text}>Bureau</Text>
      </View>
      <View>
        <Card>
          <Card.Content style={styles.textInputContainer}>
            <TextInput
              style={styles.textInput}
              mode="outlined"
              label="Mobile Number"
              placeholder="Enter your Mobile Number"
              keyboardType={'numeric'}
              value={userMobileNumber}
              onChangeText={(text) => setUserMobileNumber(text)}
            />
            <Button
              style={styles.button}
              mode="contained"
              onPress={testHandler}>
              Test
            </Button>
          </Card.Content>
        </Card>
      </View>
      {startTest ? (
        <Discover userMobileNumber={mobNum} ipAddressMob={ipAddressMob} />
      ) : null}
      {/* <MySnackbar /> */}
    </SafeAreaView>
    // </ViewSnackBar.Provider>
  );
}

const styles = StyleSheet.create({
  text: {
    padding: 5,
    fontSize: 24,
    justifyContent: 'center',
    color: 'white',
    // fontWeight: 'bold',
    // backgroundColor: '#0095c7',
  },
  header: {
    alignItems: 'center',
    alignContent: 'center',
    backgroundColor: '#2B60B2',
    padding: 5,
  },
  container: {
    flex: 1,
  },
  button: {
    marginTop: 4,
    width: '20%',
    height: 40,
    backgroundColor: '#2B60B2',
  },
  textInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  textInput: {
    width: '75%',
    height: 40,
  },
});
