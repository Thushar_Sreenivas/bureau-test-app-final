import React, {useState, useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import publicIP from 'react-native-public-ip';
import axios from 'axios';
import Initiate from './Initiate';
import {Card, Title, Subheading} from 'react-native-paper';

export default function Discover({userMobileNumber, ipAddressMob}) {
  // const ipAddress = '49.15.192.92';
  // const ipAddress = '27.97.161.170';
  const [discoverSuccess, setDiscoverSuccess] = useState(false);
  const [correlationId, setCorrelationId] = useState('');

  async function DiscoverAPICall() {
    try {
      // const ipAddress = '42.106.180.61';
      const response = await axios.get(
        `https://api-dev.bureau.id/v2/auth/discover?clientId=32be3a2e-1052-4827-93df-575276d41399&userIp=${ipAddressMob}`,
      );
      if (response.data && response.data.supported) {
        setCorrelationId(response.data.correlationId);
        console.log('Set Corelation Id at Discover', correlationId);
        setDiscoverSuccess(!discoverSuccess);
      }
    } catch (error) {
      console.warn('Error at Discovery', error.message);
    }
  }

  useEffect(() => {
    DiscoverAPICall();
  }, []);

  return (
    <View style={styles.Content}>
      <Card>
        <Card.Content>
          <Title>Discover</Title>
          <Subheading>IP Address: {ipAddressMob}</Subheading>
          <Subheading>Correlation id: {correlationId}</Subheading>
        </Card.Content>
      </Card>
      {discoverSuccess ? (
        <Initiate correlationId={correlationId} phNumber={userMobileNumber} />
      ) : null}
    </View>
  );
}
const styles = StyleSheet.create({
  Content: {
    alignContent: 'center',
    margin: 15,
    zIndex: 1,
  },
});
