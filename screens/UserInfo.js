import React, {useContext, useEffect, useState} from 'react';
import {View, Dimensions, StyleSheet} from 'react-native';
import {Card, Title, Subheading} from 'react-native-paper';
import {ViewSnackBar} from '../Context';
import axios from 'axios';

export default function UserInfo({correlationId}) {
  const [displaySnackBar, setDisplaySnackBar] = useContext(ViewSnackBar);
  const [userPhNumberFetched, setUserPhNumberFetched] = useState('');
  const [userInfoSuccess, setUserInfoSuccess] = useState(false);
  // const [loading, setloading] = useState(true);
  useEffect(() => {
    let mounted = true;
    const source = axios.CancelToken.source();
    async function userInfoFetch() {
      try {
        console.log('UserInfoFetch -> correlationId', correlationId);
        const logURL = `https://api-dev.bureau.id/v2/auth/userinfo?correlationId=${correlationId}`;
        console.log('UserInfo URL ', logURL);
        const userInfoParams = {
          method: 'get',
          url: `${logURL}`,
          headers: {
            'Content-Type': 'application/json',
            Authorization:
              'MzJiZTNhMmUtMTA1Mi00ODI3LTkzZGYtNTc1Mjc2ZDQxMzk5OjdiOTdkYmZkLTUyYzYtNDY5NC04NzAzLTI5ODlhMmY1MTlhZA==',
          },
        };
        const response = await axios.request(userInfoParams);
        console.log(
          'Userinfo response@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@',
          response,
        );

        if (response && response.data.mobileNumber) {
          if (mounted) {
            setUserPhNumberFetched(response.data.mobileNumber);
            // setDisplayUserInfo(true);
            setUserInfoSuccess(!userInfoSuccess);
          }
        }
      } catch (error) {
        console.warn('Error at UserInfo Fetch', error);
      }
    }

    userInfoFetch();
    // return () => setUserPhNumberFetched('');
    // return () => {
    //   source.cancel();
    // };
    return function cleanup() {
      mounted = false;
      source.cancel();
    };
  }, []);
  return (
    <>
      {userInfoSuccess ? (
        <View style={styles.Content}>
          <Card style={styles.Card}>
            <Card.Content>
              <Title>UserInfo </Title>
              <Subheading>
                Mobile Number Received: {userPhNumberFetched}
              </Subheading>
            </Card.Content>
          </Card>
        </View>
      ) : null}
    </>
  );
}

const styles = StyleSheet.create({
  Content: {
    marginTop: 15,
    marginBottom: 15,
  },
  Card: {
    width: 380,
    height: 100,
  },
});
