import React, {useState} from 'react';
import {View, Dimensions, StyleSheet} from 'react-native';
import {WebView} from 'react-native-webview';
const {width, height} = Dimensions.get('screen');
import {
  Card,
  Button,
  Title,
  Paragraph,
  ThemeProvider,
  Subheading,
} from 'react-native-paper';
import axios from 'axios';
import UserInfo from './UserInfo';
export default function Initiate({correlationId, phNumber}) {
  console.log('Initiate Correlation ID', correlationId);
  console.log('Initiate phone Number', phNumber);

  // const [userPhNumberFetched, setUserPhNumberFetched] = useState('');
  const [displayUserInfo, setDisplayUserInfo] = useState(false);

  const onNavigationStateChange = (navState) => {
    const event = navState.url;
    console.log('navState', navState.url);
    if (
      event.includes('https://eno5h58k519.x.pipedream.net') &&
      !event.includes('api-dev.bureau.id/v2/auth/initiate')
    ) {
      if (event.includes(correlationId)) {
        console.log('Auth Success');
        setDisplayUserInfo(!displayUserInfo);
      }
      if (event.includes('errorCode') || event.includes('errorMessage')) {
        console.log('Error Code');
        console.warn('Error at Authentication');
      }
    }
  };

  return (
    <View>
      <View>
        <WebView
          source={{
            uri: `https://api-dev.bureau.id/v2/auth/initiate?clientId=32be3a2e-1052-4827-93df-575276d41399&correlationId=${correlationId}&msisdn=${phNumber}&redirectUrl=https://eno5h58k519.x.pipedream.net`,
          }}
          javaScriptEnabled={true}
          onNavigationStateChange={(navState) =>
            onNavigationStateChange(navState)
          }
          renderError={(error, code, description) => {
            console.log(error);
            console.log(code);
            console.log(description);
          }}
        />
      </View>

      <View style={styles.Content}>
        <Card style={styles.Card}>
          <Card.Content>
            <Title>Authenticate</Title>
            <Subheading>Mobile Number Sent: {phNumber}</Subheading>
          </Card.Content>
        </Card>
        {displayUserInfo ? <UserInfo correlationId={correlationId} /> : null}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  WebViewContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    height,
    width,
    zIndex: 2,
  },
  Content: {
    marginTop: 15,
    marginBottom: 15,
  },
  Card: {
    width: 380,
    height: 100,
  },
});
