import {createContext} from 'react';

// export const URLContext = createContext([
//   'https://sheltered-taiga-67637.herokuapp.com/book/',
//   () => {},
// ]);

// export const ContextCorrelationId = createContext(['', () => {}]);
export const ViewSnackBar = createContext([false, () => {}]);
